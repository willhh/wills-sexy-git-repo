<?php


function connect()
{
    $con = mysqli_connect("localhost","root","yellowsnow", "test"); // hostAddress, username, password, database name
    if (!$con)
    {
        die('Could not connect: ' . mysqli_error());
    }
    return $con;
}

function message($string)
{
    echo "<script type='text/javascript'>";
    echo "alert('$string');";
    echo "</script>";
    
}

function redirect($path)
{
    echo "<script type='text/javascript'>";
    echo "location.href = '$path';";
    echo "</script>";
    
}

function getIP()
{ 
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) //check ip from share internet 
    { 
        $ip=$_SERVER['HTTP_CLIENT_IP']; 
    
    } 
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) //to check ip is pass from proxy 
    { 
        $ip=$_SERVER['HTTP_X_FORWARDED_FOR']; 
      
    } else { 
        $ip=$_SERVER['REMOTE_ADDR']; 
    
    } 
    return $ip; 
   
} 
?>

